import ajax from './ajax'

const BASE_URL = "http://localhost:4941/api/v1"

export const registerUser = (parameters) => {
    console.log("call register User")
    return ajax(BASE_URL + '/users/register', parameters, 'POST')
}

export const loginUser = (parameters) => {
    console.log("call login user")
    return ajax(BASE_URL + '/users/login', parameters, 'POST')
}

export const customerInfo = (user_id, token) => {
    console.log("call customer infromation")
    console.log(`${BASE_URL}/users/${user_id.toString()}`)
    let header = {headers: {'X-Authorization':token}}
    return ajax(`${BASE_URL}/users/${user_id.toString()}`,{} ,'GET', header)
}

export const getPetitions = (parameters) =>{
    console.log("get petitions information")
    return ajax(BASE_URL + '/petitions', parameters)
}

export const getPetitionPhoto = (petitionId) => {
    console.log("get petition photo")
    return ajax(BASE_URL + '/petitions/' + petitionId + '/photo')
}

export const getCategorys = () => {
    console.log("get categorys")
    return ajax(BASE_URL + '/petitions/categories')
}