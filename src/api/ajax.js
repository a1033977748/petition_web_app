import axios from 'axios'

export default function ajax(url='', parameters={}, method='GET', header={}){
    return new Promise((resolve, reject) => {
        let promise
        if(method === 'GET'){
            // join parameters
            let parametersStr = ''
            Object.keys(parameters).forEach(key => {
                parametersStr += key + '=' + parameters[key] + '&'
            })

            // join to full parameters
            if (parametersStr !== '') {
                parametersStr = parametersStr.substr(0, parametersStr.lastIndexOf('&'))
                url = url + '?' + parametersStr;
            }

            // Send GET request
            promise = axios.get(url, header)

        } else {
            if (method == 'POST'){
                promise = axios.post(url, parameters, header)
            } else if (method == 'PUT') {
                promise = axios.put(url, parameters, header)
            } else if (method == 'DELETE') {
                promise = axios.delete(url, header)
            } else {
                promise = axios.patch(url, parameters, header)
            }
        }

        promise.then(response => {
            resolve(response.data)
        })
            .catch(error => {
                reject(error)
            });
    })
}