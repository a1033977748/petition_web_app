import {
    registerUser, 
    loginUser, 
    customerInfo,
    getPetitions,
    getCategorys
    } 
from '../api/api'

import {USER_ID, LOGIN_USER, PETITIONS_INFO, CATEGORYS_INFO} from './mutation-types'

const BASE_URL = "http://localhost:4941/api/v1"

export default {
    async userRegister({commit}, params) {
        console.log("actions")
        const userId = await registerUser(params)
        commit(USER_ID, {user_id: userId})
    },
    async loginUser({commit}, params){
        const login_user = await loginUser(params)
        const customer_info = await customerInfo(login_user.userId, login_user.token)
        const user_infro = {
            token:login_user.token,
            user_id:login_user.userId,
            name:customer_info.name,
            email:customer_info.email
        }

        console.log(user_infro)
        commit(LOGIN_USER, {login_user: user_infro})
    },
    async getPetitions({commit}, params){
        const get_petition = await getPetitions(params)
        get_petition.forEach(async petititon => {
            petititon.image_url = BASE_URL + '/petitions/' + petititon.petitionId + '/photo';
        });
        commit(PETITIONS_INFO, {petitions_info: get_petition})
    },
    async getCategorys({commit}, params){
        const categorys = await getCategorys(params)
        commit(CATEGORYS_INFO, {categorys_info: categorys})
    }
}
    