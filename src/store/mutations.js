import {USER_ID} from './mutation-types'
import {LOGIN_USER} from './mutation-types'
import {PETITIONS_INFO} from './mutation-types'
import {CATEGORYS_INFO} from  './mutation-types'

export default{
    [USER_ID](state, {user_id}){
        state.user_id = user_id
    },
    [LOGIN_USER](state, {login_user}){
        state.login_user = login_user
    },
    [PETITIONS_INFO](state, {petitions_info}){
        state.petitions_info = petitions_info
    },
    [CATEGORYS_INFO](state, {categorys_info}){
        state.categorys_info = categorys_info
    }
}