import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '../pages/login/Login';
import Petition from '../pages/petition/Petition';

Vue.use(VueRouter)

export default new VueRouter({
    routes:[
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/petition',
            name: 'petition',
            component: Petition
        }
    ]
})